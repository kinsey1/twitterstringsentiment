from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn import tree
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_fscore_support

from collections import Counter


import matplotlib.pyplot as plt
import numpy as np
import csv
import conf

from sklearn.feature_selection import VarianceThreshold

STACKING = True
MOST = False
TOP = False

def classify(classifer,x,y,test_x):
    pred = classifer.fit(x, y)
    y_predicted = pred.predict(test_x)
    return y_predicted

def get_data(filename):
    csv_trainfile =  open(filename, 'r')
    train_data = list(csv.reader(csv_trainfile, delimiter = ','))
    train_id = [x[0] for x in train_data]
    x = np.array([np.asfarray(x[1:-1]) for x in train_data])
    y = [x[-1] for x in train_data]

    return (x, y)

# csv_trainfile =  open('data/train_top10.csv', 'r')
# train_data = list(csv.reader(csv_trainfile, delimiter = ','))
# train_id = [x[0] for x in train_data]
# x = np.array([np.asfarray(x[1:-1]) for x in train_data])
# y = [x[-1] for x in train_data]



# csv_testfile = open('data/dev_top10.csv')
# test_data = list(csv.reader(csv_testfile, delimiter = ','))
# test_x = np.array([np.asfarray(x[1:-1]) for x in test_data])
# test_y = [x[-1] for x in test_data]

train_set_top = get_data("data/train_top10.csv")
train_set_most = get_data("data/train_most100.csv")

# x = train_set_top[0]



test_set_top = get_data("data/dev_top10.csv")
test_set_most = get_data("data/dev_most100.csv")
# x_test = test_set[0]
# y_test = test_set[1]

# Are we using both datasets combined?
if(STACKING):
    x = np.hstack((train_set_top[0],train_set_most[0]))
    x_test = np.hstack((test_set_top[0], test_set_most[0]))
    y_test = test_set_top[1]
    y = train_set_top[1]

if(MOST):
    x = train_set_most[0]
    y = train_set_most[1]
    x_test = test_set_most[0]
    y_test = test_set_most[1]

if(TOP):
    x = train_set_top[0]
    y = train_set_top[1]
    x_test = test_set_top[0]
    y_test = test_set_top[1]


#print(np.var(x[:,0]))
#exit(0)


#predicted = classify(tree.DecisionTreeClassifier(criterion="gini"), x, y, x_test)
#predicted = classify(svm.SVC(), x, y, x_test)
#predicted = np.asarray(['FacePalm' for x in range(0,len(y_test))])
#predicted = classify(RandomForestClassifier(n_estimators=30), x, y, x_test)
#predicted = classify(GaussianNB(), x, y, x_test)
#predicted = classify(KNeighborsClassifier(n_neighbors=5), x, y, x_test)

#print(x_test[0:30])
print(predicted[30:50])
print(y_test[30:50])

33

print("accuracy:")
print((predicted==y_test).sum()/float(len(y_test)))
print((predicted==y_test).sum())
print(len(y_test))
classes = sorted(list(set(predicted)))
print("precision, recall, fscore")
print(precision_recall_fscore_support(y_test, predicted, average = 'macro'))
# Compute confusion matrix
print(len(predicted))

cnf_matrix = confusion_matrix(y_test, predicted, labels=classes)
np.set_printoptions(precision=2)

print(Counter(predicted))

print("ACTUAL")
print(Counter(y_test))

# Plot non-normalized confusion matrix
plt.figure()
conf.plot_confusion_matrix(cnf_matrix,classes = classes,
                      title='Random Forest Classifier, Stacked Dataset')
plt.show()
